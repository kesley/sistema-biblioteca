package br.com.sistema.biblioteca.dados;

import br.com.sistema.biblioteca.modelo.Livro;

import java.util.ArrayList;

public class LivroDAO {

    private static final ArrayList<Livro> livros = new ArrayList<>();

    public static void adicionarLivro(Livro livro) { livros.add(livro); }

    public static Livro retornaLivro(int pesquisa) {

        for (Livro livro : livros) {
            if(livro.getIsbn() == pesquisa)
                return livro;
        }

        return null;
    }
}
