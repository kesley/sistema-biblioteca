package br.com.sistema.biblioteca.dados;

import br.com.sistema.biblioteca.modelo.Usuario;

import java.util.ArrayList;

public class UsuarioDAO {

    private static final ArrayList<Usuario> usuarios = new ArrayList<>();

    public static void adicionarUsuario(Usuario usuario) {
        usuarios.add(usuario);
    }

    public static Usuario retornaUsuario(String cpf) {

        for (Usuario usuario : usuarios) {
            if (usuario.getCpf().equals(cpf)) {
                return usuario;
            }
        }

        return null;
    }

    public static void removerUsuario(Usuario usuario) {

        usuarios.removeIf(pesquisaUsuario -> pesquisaUsuario == usuario);
    }
}
