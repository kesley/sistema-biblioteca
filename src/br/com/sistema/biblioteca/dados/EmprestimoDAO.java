package br.com.sistema.biblioteca.dados;

import br.com.sistema.biblioteca.modelo.Emprestimo;

import java.util.ArrayList;

public class EmprestimoDAO {

    private static final ArrayList<Emprestimo> emprestimos = new ArrayList<>();


    public static void adicionarEmprestimo(Emprestimo emprestimo) { emprestimos.add(emprestimo); }

    public static ArrayList<Emprestimo> getAllEmprestimosAluno(String cpf) {

        ArrayList<Emprestimo> emprestimosEspecificoAluno = new ArrayList<>();

        for (Emprestimo emprestimo : emprestimos) {
            if (emprestimo.getCpfAluno().equals(cpf))
                emprestimosEspecificoAluno.add(emprestimo);
        }

        return emprestimosEspecificoAluno;
    }

    public static Emprestimo getEmprestimo(String cpfAluno, int isbnLivro) {

        for (Emprestimo emprestimo : emprestimos) {
            if (emprestimo.getCpfAluno().equals(cpfAluno) && emprestimo.getIsbnLivro() == (isbnLivro)) {
                return emprestimo;
            }
        }

        return null;
    }

    public static void removeEmprestimo(Emprestimo emprestimo) {

        emprestimos.removeIf(emprestimoBusca -> emprestimoBusca == emprestimo);

    }
}
