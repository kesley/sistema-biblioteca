package br.com.sistema.biblioteca.modelo;


public class Livro {

    private final int isbn;
    private final int edicao;
    private int qtdExemplares;
    private final String autores;
    private final String titulo;
    private final String editora;
    private final String assunto;


    public Livro(int isbn, int edicao, int qtdExemplares, String autores, String titulo, String editora, String assunto) {

        this.qtdExemplares = qtdExemplares;

        this.isbn = isbn;
        this.edicao = edicao;
        this.autores = autores;
        this.titulo = titulo;
        this.editora = editora;
        this.assunto = assunto;
    }

    public Livro(int isbn, int edicao, String autores, String titulo, String editora, String assunto) {
        this.qtdExemplares = 0;

        this.isbn = isbn;
        this.edicao = edicao;
        this.autores = autores;
        this.titulo = titulo;
        this.editora = editora;
        this.assunto = assunto;

    }

    public void addExemplar(int qtd) {
        this.qtdExemplares += qtd;
    }
    public void rmExemplar() { this.qtdExemplares--; }

    public int getQtdExemplares() { return qtdExemplares; }
    public String getAssunto() { return assunto; }
    public String getAutores() { return autores; }
    public String getEditora() { return editora; }
    public String getTitulo() { return titulo; }
    public int getEdicao() { return edicao; }
    public int getIsbn() { return isbn; }



}
