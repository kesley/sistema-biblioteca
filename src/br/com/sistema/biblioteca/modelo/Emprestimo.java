package br.com.sistema.biblioteca.modelo;

public class Emprestimo {

    private final int isbnLivro;
    private final String cpfAluno;

    public Emprestimo(int isbn, String cpf) {
        this.isbnLivro = isbn;
        this.cpfAluno = cpf;
    }

    public String getCpfAluno() { return cpfAluno; }
    public int getIsbnLivro() { return isbnLivro; }

}
