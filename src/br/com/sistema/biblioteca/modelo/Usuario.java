package br.com.sistema.biblioteca.modelo;

public abstract class Usuario  {

    private final String nome;
    private final String senha;
    private final String cpf;
    private boolean isLogged;

    Usuario(String nome, String cpf, String senha) {
        this.nome = nome;
        this.senha = senha;
        this.cpf = cpf;
        this.isLogged = false;
    }

    // getters
    public String getNome() { return this.nome; }
    public String getCpf() { return this.cpf; }
    public boolean isLogged() { return this.isLogged; }

    public void beOnline() { this.isLogged = true; }
    public void beOffline() {this.isLogged = false;}


    public boolean autenticaSenha(String senha) { return this.senha.equals(senha); }

}
