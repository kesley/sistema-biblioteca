package br.com.sistema.biblioteca.modelo;

public class Aluno extends Usuario {

    private int qtdEmprestimo;

    public Aluno(String nome, String cpf, String senha) {
        super(nome, cpf, senha);

        this.qtdEmprestimo = 0;
    }

    public int getQtdEmprestimo() {
        return this.qtdEmprestimo;
    }
    public void addEmprestimo() { this.qtdEmprestimo++; }
    public void rmEmprestimo() { this.qtdEmprestimo--; }

}
