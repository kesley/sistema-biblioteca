package br.com.sistema.biblioteca.apresentacao;

import br.com.sistema.biblioteca.controle.*;
import br.com.sistema.biblioteca.modelo.Aluno;
import br.com.sistema.biblioteca.modelo.Funcionario;
import br.com.sistema.biblioteca.modelo.Livro;
import br.com.sistema.biblioteca.modelo.Usuario;

import java.io.IOException;
import java.util.Scanner;

public class SistemaBiblioteca {

    public static void main(String[] args) throws IOException {

        Scanner read = new Scanner(System.in);

        boolean mainLoop = true;
        int opcao;

        while(mainLoop) {

            menus(1);
            opcao = Integer.parseInt(read.nextLine());

            switch (opcao) {
                case 1 -> {
                    Usuario usuario = fazerLogin(read);
                    if (usuario != null) {
                        System.out.println("Login realizado com sucesso!");

                        usuario.beOnline();

                        if (usuario instanceof Aluno)
                            alunoLogado(usuario, read);
                        else if (usuario instanceof Funcionario)
                            funcionarioLogado(usuario, read);
                        else
                            adiministradorLogado(usuario, read);

                    } else {
                        System.out.println("Falha no login!");
                    }
                }
                case 2 -> gerenciadorImportacoes(read);
                case 3 -> mainLoop = false;
            }
        }
    }

    private static void gerenciadorImportacoes(Scanner read) throws IOException {
        System.out.println("========== Importacao de informacoes ===========");

        ImportacoesControlador ic = new ImportacoesControlador();

        boolean flagImportacao = true;
        int opcao;

        while(flagImportacao) {
            menus(5);
            opcao = Integer.parseInt(read.nextLine());

            switch (opcao) {
                case 1 -> ic.importarUsuarios();
                case 2 -> ic.importarLivros();
                case 3 -> flagImportacao = false;
            }
        }
    }

    private static void funcionarioLogado(Usuario usuario, Scanner read) {

        FuncionarioControlador fc = new FuncionarioControlador();

        boolean flagFuncionario = true;
        int opcao;

        while(flagFuncionario) {
            System.out.printf("========== Funcionario Logado ========== - %s\n", usuario.getNome());
            menus(3);
            opcao = Integer.parseInt(read.nextLine());

            switch (opcao) {
                case 1 -> fc.fazerEmprestimo();
                case 2 -> fc.receberEmprestimo();
                case 3 -> fc.cadastrarLivro();
                case 4 -> fc.adicionarExemplares();
                case 5 -> buscarLivro(read);
                case 6 -> {
                    flagFuncionario = false;
                    usuario.beOffline();
                }
            }
        }
    }

    private static void alunoLogado(Usuario usuario, Scanner read) {

        AlunoControlador ac = new AlunoControlador();

        boolean flagAluno = true;
        int opcao;

        while(flagAluno) {
            System.out.printf("========== Aluno logado ========== - %s\n", usuario.getNome());
            menus(2);
            opcao = Integer.parseInt(read.nextLine());

            switch (opcao) {
                case 1 -> ac.mostrarEmprestimos((Aluno) usuario);
                case 2 -> buscarLivro(read);
                case 3 -> {
                    flagAluno = false;
                    usuario.beOffline();
                }
            }
        }
    }

    private static void adiministradorLogado(Usuario usuario, Scanner read) {

        System.out.printf("=========== Adiministrador Logado ========== - %s\n", usuario.getNome());
        AdiministradorControlador ac = new AdiministradorControlador();

        boolean flagAdm = true;
        int opcao;

        while(flagAdm) {

            menus(4);
            opcao = Integer.parseInt(read.nextLine());

            switch (opcao) {
                case 1 -> ac.cadastrarUsuario();
                case 2 -> ac.removeUsuario(read);
                case 3 -> {
                    flagAdm = false;
                    usuario.beOffline();
                }
            }
        }
    }

    private static Usuario fazerLogin(Scanner read) {

        LoginControlador lc = new LoginControlador();

        System.out.print("Insira o cpf: ");
        String cpf = read.nextLine();

        System.out.print("Insira a senha: ");
        String senha = read.nextLine();

        return lc.autenticarUsuario(cpf, senha);

    }

    private static void buscarLivro(Scanner read) {

        LivroControlador lc = new LivroControlador();

        System.out.println("========== Buscar Livro ==========");
        System.out.print("Insira o ISBN do livro: ");
        int isbnBusca = Integer.parseInt(read.nextLine());

        Livro livro = lc.buscaLivro(isbnBusca);

        if(livro != null) {
            System.out.printf("Titulo: %s\n", livro.getTitulo());
            System.out.printf("Edicao: %s\n", livro.getEdicao());
            System.out.printf("Editora: %s\n", livro.getEditora());
            System.out.printf("Assunto: %s\n", livro.getAssunto());
            System.out.printf("Autore(s) %s\n", livro.getAutores());
            System.out.printf("Exemplares: %d\n", livro.getQtdExemplares());
        } else {
            System.out.println("Livro nao Encontrado!");
        }
    }

    public static void menus(int menu) {

        switch (menu) {
            case 1 -> {
                System.out.println("[ 1 ] - Fazer login");
                System.out.println("[ 2 ] - Importar informacoes");
                System.out.println("[ 3 ] - Sair");
            }
            case 2 -> {
                System.out.println("[ 1 ] - Verificar emprestimos");
                System.out.println("[ 2 ] - Buscar livro");
                System.out.println("[ 3 ] - deslogar");
            }
            case 3 -> {
                System.out.println("[ 1 ] - Fazer emprestimo");
                System.out.println("[ 2 ] - Receber emprestimo");
                System.out.println("[ 3 ] - Cadastrar livro");
                System.out.println("[ 4 ] - Adicionar Exemplares");
                System.out.println("[ 5 ] - Buscar livro");
                System.out.println("[ 6 ] - deslogar");
            }
            case 4 -> {
                System.out.println("[ 1 ] - Cadastrar usuario");
                System.out.println("[ 2 ] - Remover usuario");
                System.out.println("[ 3 ] - deslogar");
            }
            case 5 -> {
                System.out.println("[ 1 ] - Importar usuarios");
                System.out.println("[ 2 ] - Importar livros");
                System.out.println("[ 3 ] - voltar");
            }
        }

        System.out.print("Opcao: ");
    }

}
