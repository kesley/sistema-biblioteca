package br.com.sistema.biblioteca.controle;

import br.com.sistema.biblioteca.modelo.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class ImportacoesControlador {

    private final String filepath = "src/br/com/sistema/biblioteca/importacoes/";

    public void importarUsuarios() throws IOException {

        BufferedReader br = new BufferedReader(new FileReader(filepath + "usuarios.csv"));

        UsuarioControlador uc = new UsuarioControlador();

        Usuario usuario = null;

        String line;
        while ((line = br.readLine()) != null) {


            String[] info = line.split(",");

            if(uc.buscaUsuario(info[1]) != null) {
                System.out.printf("Usuario %s ja cadastrado!", info[0]);
                continue;
            }

            switch (info[3]) {
                case "1" -> usuario = new Aluno(info[0], info[1], info[2]);
                case "2" -> usuario = new Funcionario(info[0], info[1], info[2]);
                case "3" -> usuario = new Adiministrador(info[0], info[1], info[2]);
            }

            uc.cadastrarUsuario(usuario);

            System.out.printf("Usuario: %s, importado com sucesso!\n", info[0]);

        }
    }

    public void importarLivros() throws IOException {

        BufferedReader br = new BufferedReader(new FileReader(filepath + "livros.csv"));

        LivroControlador lc = new LivroControlador();

        Livro livro;
        int qtdExemplares;

        String line;
        while((line = br.readLine()) != null) {

            String[] info = line.split(",");

            if(lc.buscaLivro(Integer.parseInt(info[0])) != null) {
                System.out.println("Livro ja cadastrado!");
                continue;
            }

            qtdExemplares = Math.max(Integer.parseInt(info[2]), 0);

            livro = new Livro(Integer.parseInt(info[0]), Integer.parseInt(info[1]), qtdExemplares, info[3], info[4], info[5], info[6]);
            lc.cadastrarLivro(livro);

            System.out.printf("Livro: %s, importado com sucesso!\n", info[4]);

        }
    }

}
