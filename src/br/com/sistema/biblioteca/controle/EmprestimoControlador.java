package br.com.sistema.biblioteca.controle;


import br.com.sistema.biblioteca.dados.EmprestimoDAO;
import br.com.sistema.biblioteca.modelo.Emprestimo;

import java.util.ArrayList;

public class EmprestimoControlador {

    public void cadastrarEmprestimo(Emprestimo emprestimo) { EmprestimoDAO.adicionarEmprestimo(emprestimo); }

    public Emprestimo buscaEmprestimo(String cpfAluno, int isbnLivro) { return EmprestimoDAO.getEmprestimo(cpfAluno, isbnLivro); }

    public ArrayList<Emprestimo> retornaListaEmprestimos(String cpf) {

        return EmprestimoDAO.getAllEmprestimosAluno(cpf);

    }

    public void removerEmprestimo(Emprestimo emprestimo) {

        EmprestimoDAO.removeEmprestimo(emprestimo);

    }
}
