package br.com.sistema.biblioteca.controle;

import br.com.sistema.biblioteca.modelo.Aluno;
import br.com.sistema.biblioteca.modelo.Emprestimo;
import br.com.sistema.biblioteca.modelo.Livro;
import br.com.sistema.biblioteca.modelo.Usuario;

import java.util.Scanner;

public class FuncionarioControlador {

    private final Scanner read = new Scanner(System.in);

    private final UsuarioControlador uc = new UsuarioControlador();
    private final EmprestimoControlador ec = new EmprestimoControlador();
    private final LivroControlador lc = new LivroControlador();

    private Livro livro = null;


    public void fazerEmprestimo() {
        System.out.print("Insira o cpf do aluno: ");
        String cpfAluno = read.nextLine();

        Usuario usuario = uc.buscaUsuario(cpfAluno);

        if (usuario == null) {
            System.out.println("Esuario nao existe no sistema!");
            return;
        } else if (!(usuario instanceof Aluno)) {
            System.out.println("Somente alunos podem fazer emprestimo de livros!");
            return;
        }

        Aluno aluno = (Aluno) usuario;

        if (aluno.getQtdEmprestimo() == 2) {
            System.out.println("Aluno ja tem 2 emprestimo!");
            return;
        }

        System.out.print("Insira o isbn do livro: ");
        int isbnEmprestimo = Integer.parseInt(read.nextLine());

        livro = lc.buscaLivro(isbnEmprestimo);

        if (livro == null) {
            System.out.println("Livro nao encontrado!");
            return;
        } else if(livro.getQtdExemplares() == 0) {
            System.out.println("Nao pode fazer emprestimo de um livro sem exemplares!");
        }

        aluno.addEmprestimo();
        livro.rmExemplar();

        Emprestimo emprestimo = new Emprestimo(isbnEmprestimo, cpfAluno);
        ec.cadastrarEmprestimo(emprestimo);

        System.out.println("Emprestimo realizado!");

    }

    public void receberEmprestimo() {
        System.out.print("Insira o cpf do aluno a devolver um livro: ");
        String cpfAluno = read.nextLine();

        Usuario usuario = uc.buscaUsuario(cpfAluno);

        if (usuario == null) {
            System.out.println("Aluno nao cadastrado no sistema!");
            return;
        } else if(!(usuario instanceof Aluno)) {
            System.out.println("CPF informado nao e o de um aluno!");
            return;
        }

        if (((Aluno) usuario).getQtdEmprestimo() == 0) {
            System.out.println("Aluno nao tem emprestimos a ser recebido!");
            return;
        }

        System.out.print("Insira o isbn do livro a ser recebido");
        int isbnLivro = Integer.parseInt(read.nextLine());

        Livro livro = lc.buscaLivro(isbnLivro);

        if (livro == null) {
            System.out.println("Livro nao cadastrado no sistema");
            return;
        }

        Emprestimo emprestimo = ec.buscaEmprestimo(cpfAluno, isbnLivro);

        if (emprestimo == null) {
            System.out.println("Este aluno nao tem nenhum emprestimo desse livro!");
            return;
        }

        livro.addExemplar(1);
        ((Aluno) usuario).rmEmprestimo();

        ec.removerEmprestimo(emprestimo);

        System.out.println("Emprestimo recebido!");
    }

    public void cadastrarLivro() {
        System.out.println("========== Cadastrar Livro ==========");
        System.out.print("Insira o ISBN: ");
        int isbnCad = Integer.parseInt(read.nextLine());
        System.out.print("Insira a edicao: ");
        int edicao = Integer.parseInt(read.nextLine());
        System.out.print("Insira os autores: ");
        String autores = read.nextLine();
        System.out.print("Insira o titulo: ");
        String titulo = read.nextLine();
        System.out.print("Insira a editora: ");
        String editora = read.nextLine();
        System.out.print("Insira o assunto: ");
        String assunto = read.nextLine();

        livro = new Livro(isbnCad, edicao, autores, titulo, editora, assunto);
        lc.cadastrarLivro(livro);

        System.out.println("Livro cadastrado!");
    }

    public void adicionarExemplares() {
        System.out.println("========== Adicionar Exemplar ==========");
        System.out.print("Insira o ISBN do livro: ");
        int isbnAdd = Integer.parseInt(read.nextLine());

        System.out.print("Insira a quantidade novos exemplares: ");
        int qtd_exemplar = Integer.parseInt(read.nextLine());

        if(!lc.adicionarExemplar(isbnAdd, qtd_exemplar)) {
            System.out.println("Nao foi possivel localizar o livro!");
            return;
        }

        System.out.println("Exemplar adicionado com sucesso!");

    }

}
