package br.com.sistema.biblioteca.controle;


import br.com.sistema.biblioteca.modelo.Usuario;

public class LoginControlador {

    public Usuario autenticarUsuario(String cpf, String senha) {

        UsuarioControlador uc = new UsuarioControlador();

        Usuario usuario = uc.buscaUsuario(cpf);

        if (usuario != null) {

            if (usuario.autenticaSenha(senha)) {
                return usuario;
            }
        }

        return null;
    }
}
