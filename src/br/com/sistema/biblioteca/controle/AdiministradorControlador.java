package br.com.sistema.biblioteca.controle;

import br.com.sistema.biblioteca.modelo.Aluno;
import br.com.sistema.biblioteca.modelo.Funcionario;
import br.com.sistema.biblioteca.modelo.Usuario;

import java.util.Scanner;

public class AdiministradorControlador {

    private final Scanner read = new Scanner(System.in);
    private final UsuarioControlador uc = new UsuarioControlador();

    public void cadastrarUsuario() {

        System.out.print("tipo de usuario: [0: Aluno / 1: Funcionario]: ");
        int tipo = Integer.parseInt(read.nextLine());

        System.out.print("Insira o nome do usuario: ");
        String nome = read.nextLine();

        System.out.print("Insira o cpf do usuario: ");
        String cpf = read.nextLine();

        if (uc.buscaUsuario(cpf) != null) {
            System.out.println("Usuario ja cadastrado!" );
            return;
        }

        System.out.print("Insira uma senha para o usuario: ");
        String senha = read.nextLine();

        Usuario usuario;
        if (tipo == 0) {
            usuario = new Aluno(nome, cpf, senha);
        } else {
            usuario = new Funcionario(nome, cpf, senha);
        }

        uc.cadastrarUsuario(usuario);

        System.out.println("Cadastro realizado!");
    }

    public void removeUsuario(Scanner read) {

        System.out.print("Insira o cpf de usuario a ser removido: ");
        String cpf = read.nextLine();

        Usuario usuario = uc.buscaUsuario(cpf);

        if (usuario == null) {
            System.out.println("Usuario nao encontrado no sistema!");
            return;
        }

        if (usuario instanceof Aluno) {
            if(((Aluno) usuario).getQtdEmprestimo() != 0) {
                System.out.println("Aluno contem emprestimos no sistema, nao pode ser removido!");
            }
        }

        if (usuario.isLogged()){
            System.out.println("Usuario esta logado no sistema!");
            return;
        }

        uc.removeUsuario(usuario);

        System.out.println("Usuario removido!");
    }
}
