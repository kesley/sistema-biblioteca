package br.com.sistema.biblioteca.controle;


import br.com.sistema.biblioteca.dados.LivroDAO;
import br.com.sistema.biblioteca.modelo.Livro;

public class LivroControlador {

    public void cadastrarLivro(Livro livro) { LivroDAO.adicionarLivro(livro); }

    public Livro buscaLivro(int pesquisa) {

        return LivroDAO.retornaLivro(pesquisa);

    }

    public boolean adicionarExemplar(int isbn, int qtd) {

        Livro livro = LivroDAO.retornaLivro(isbn);

        if(livro == null)
            return false;

        livro.addExemplar(qtd);

        return true;
    }
}
