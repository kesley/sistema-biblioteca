package br.com.sistema.biblioteca.controle;


import br.com.sistema.biblioteca.dados.UsuarioDAO;
import br.com.sistema.biblioteca.modelo.Usuario;

public class UsuarioControlador {

    public void cadastrarUsuario(Usuario usuario) {
        UsuarioDAO.adicionarUsuario(usuario);
    }

    public void removeUsuario(Usuario usuario) { UsuarioDAO.removerUsuario(usuario); }

    public Usuario buscaUsuario(String cpf) {
        return UsuarioDAO.retornaUsuario(cpf);
    }

}
