package br.com.sistema.biblioteca.controle;

import br.com.sistema.biblioteca.modelo.Aluno;
import br.com.sistema.biblioteca.modelo.Emprestimo;
import br.com.sistema.biblioteca.modelo.Livro;

import java.util.ArrayList;

public class AlunoControlador {

    EmprestimoControlador ec = new EmprestimoControlador();
    LivroControlador lc = new LivroControlador();

    ArrayList<Emprestimo> emprestimosAluno = new ArrayList<>();

    public void mostrarEmprestimos(Aluno aluno) {

        System.out.printf("Voce tem atualmente %d emprestimos no sistema.\n", aluno.getQtdEmprestimo());

        emprestimosAluno = ec.retornaListaEmprestimos(aluno.getCpf());

        Livro livro;
        for (Emprestimo emprestimo : emprestimosAluno) {
            livro = lc.buscaLivro(emprestimo.getIsbnLivro());

            System.out.printf("Livro: %s \n", livro.getTitulo());
        }
    }
}
